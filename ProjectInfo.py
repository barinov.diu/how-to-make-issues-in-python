from datetime import date, timedelta, datetime
import os

import gitlab as gb
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors
from matplotlib.backends.backend_pdf import PdfPages as pdf 

from main_constants import *

def Date_From_Str(str_date):
    return date(int(str_date.split('-')[0]), int(str_date.split('-')[1]), int(str_date.split('-')[2]))

class ProjectInfo:
    def __init__(self, project):
        self.project = project
        self.__max_milestone_id = project.milestones.list()[0].iid
        self.__max_issue_id = project.issues.list()[0].iid
        self.__cnt_of_pages = self.__max_issue_id // 20 + 2
        self.start_date = Date_From_Str(PROJECT_START_DATE)
        self.date_today = Date_From_Str(datetime.today().strftime('%Y-%m-%d'))
        self.__Init_All()
        
        if not os.path.exists(REPO_PATH + "Images"):
            os.mkdir(REPO_PATH + "Images")
        
        pdf_today_name = REPO_PATH + "Images/graphs_" + self.date_today.strftime('%Y-%m-%d') + "_"
        
        if not os.path.exists(pdf_today_name + "1" + ".pdf"):
            self.pdf_name = pdf_today_name + "1" + ".pdf"
            self.pdf = pdf(self.pdf_name, keep_empty=False)
        else:
            num = 2
            while os.path.exists(pdf_today_name + str(num) + ".pdf"):
                num += 1
               
            self.pdf_name = pdf_today_name + str(num) + ".pdf"
            self.pdf = pdf(self.pdf_name, keep_empty=False)
          
    def __Init_Milestones(self):
        self.milestones = []
        for i in range(1, self.__max_milestone_id + 1):
            buf = self.project.milestones.list(page=i)
            for k in buf:
                self.milestones.append(k)
                
        self.milestones.reverse()
        
    def __Init_Milestone_Issues(self):
        self.milestone_issues = []
        for elem in self.milestones:
            self.milestone_issues.append(self.project.issues.list(milestone=elem.title))

    def __Init_Issues(self):
        self.issues = []
        for i in range(1, self.__cnt_of_pages + 1):
            buf = self.project.issues.list(page=i)
            for k in buf:
                self.issues.append(k)

        self.issues.reverse()
        
    def __Init_Issues_Ids(self):
        self.issues_ids = []

        for i in self.issues:
            self.issues_ids.append(i.iid)
    
    def __Init_Issues_Numbres(self):
        self.issues_numbers = dict()

        for cnt, elem in enumerate(self.issues_ids):
            self.issues_numbers[elem] = cnt
            
    def __Init_Percent_All_Issues_Done(self):
        self.percent_all_issues_done = dict()

        for elem in self.issues:
            time_stats = elem.attributes["time_stats"]

            estimate = time_stats["time_estimate"]
            spent    = time_stats["total_time_spent"]

            if spent == 0:
                self.percent_all_issues_done[elem.iid] = 0
            else:
                self.percent_all_issues_done[elem.iid] = round ((float(spent) / float(estimate)) * 100, 2)
                
    def __Init_Issues_Titles(self):
        self.issues_titles = dict()

        for elem in self.issues:
            self.issues_titles[elem.iid] = elem.title
            
    def __Init_Open_Issues(self):
        self.open_issues = [] # True if open, False otherwise

        for i in range(len(self.issues_ids)):
            if self.issues[i].attributes["state"] == "opened":
                self.open_issues.append(True)
            else:
                self.open_issues.append(False)
                
    def __Init_Open_X_Closed_Percent_Done(self):
        self.percent_open_issues_done   = dict()
        self.percent_closed_issues_done = dict()

        for cnt, elem in enumerate(self.percent_all_issues_done):
            if self.open_issues[cnt]:
                self.percent_open_issues_done[elem]   = self.percent_all_issues_done[elem]
            else:
                self.percent_closed_issues_done[elem] = self.percent_all_issues_done[elem]
                
    def __Init_Create_At_X_Close_At(self):
        self.issues_created_at = []
        self.issues_closed_at  = []

        for i in self.issues:
            self.issues_created_at.append(i.created_at[0:10])
            if self.open_issues[self.issues_numbers[i.iid]] == False:
                self.issues_closed_at.append(i.closed_at[0:10])
            
    def __Init_Dates(self):
        start_date = Date_From_Str(PROJECT_START_DATE)
        
        self.dates = [] # dates from PROJECT_START_DATE to today (inclusive)

        delta = self.date_today - start_date
        if delta.days <= 0:
            print ("Error with datetime.today()")
        else:
            for i in range(delta.days + 1):
                self.dates.append ((start_date + timedelta(i)).strftime('%Y-%m-%d'))

                
    milestones        = []
    milestone_issues  = []
    issues            = []
    issues_ids        = []
    open_issues       = []
    issues_created_at = []
    issues_closed_at  = []
    all_graphs        = []
    dates             = []
    issues_numbers             = dict()
    issues_titles              = dict()
    percent_all_issues_done    = dict()
    percent_open_issues_done   = dict()
    percent_closed_issues_done = dict()
    __max_issue_id   = 0
    __cnt_of_pages   = 0
    pdf_was_closed   = False
                
    def __Init_All(self):
        self.__Init_Milestones()
        self.__Init_Milestone_Issues()
        self.__Init_Issues()
        self.__Init_Issues_Ids()
        self.__Init_Issues_Numbres()
        self.__Init_Percent_All_Issues_Done()
        self.__Init_Issues_Titles()
        self.__Init_Open_Issues()
        self.__Init_Open_X_Closed_Percent_Done()
        self.__Init_Create_At_X_Close_At()
        self.__Init_Dates()
        
    def Print_All_Members(self):
        if COLOR_CONSOLE_SUPPORTED:
            Pr_Red ("Milestones = ") 
            Pr_Bla(self.milestones)
            print("\n")
            Pr_Red ("Milestone issues = ") 
            Pr_Bla(self.milestone_issues)
            print("\n")
            Pr_Red ("Issues = ") 
            Pr_Bla(self.issues)
            print("\n")
            Pr_Red ("issues_ids = ") 
            Pr_Bla(self.issues_ids)
            print("\n")
            Pr_Red ("open_issues = ")
            Pr_Bla(self.open_issues)
            print("\n")
            Pr_Red ("issues_numbers = ") 
            Pr_Bla(self.issues_numbers)
            print("\n")
            Pr_Red ("issues_titles = ")
            Pr_Bla(self.issues_titles)
            print("\n")
            Pr_Red ("percent_all_issues_done = ")
            Pr_Bla(self.percent_all_issues_done)
            print("\n")
            Pr_Red ("percent_open_issues_done = ")
            Pr_Bla(self.percent_open_issues_done)
            print("\n")
            Pr_Red ("percent_closed_issues_done = ")
            Pr_Bla(self.percent_closed_issues_done)
            print("\n")
            Pr_Red ("issues_created_at = ")
            Pr_Bla(self.issues_created_at)
            print("\n")
            Pr_Red ("issues_closed_at = ")
            Pr_Bla(self.issues_closed_at)
            print("\n")
            Pr_Red ("max_issue_id = ")
            Pr_Bla(self.__max_issue_id)
            print("\n")
            Pr_Red ("cnt_of_pages = ")
            Pr_Bla(self.__cnt_of_pages)
        else:
            print("Milestones = ",                 self.milestones)
            print("Milestone issues = ",           self.milestone_issues)
            print("Issues = ",                     self.issues)
            print("issues_ids = ",                 self.issues_ids)
            print("open_issues = ",                self.open_issues)
            print("issues_numbers = ",             self.issues_numbers)
            print("issues_titles = ",              self.issues_titles)
            print("percent_all_issues_done = ",    self.percent_all_issues_done)
            print("percent_open_issues_done = ",   self.percent_open_issues_done)
            print("percent_closed_issues_done = ", self.percent_closed_issues_done)
            print("issues_created_at = ",          self.issues_created_at)
            print("issues_closed_at = ",           self.issues_closed_at)
            print("max_issue_id = ",               self.__max_issue_id)
            print("cnt_of_pages = ",               self.__cnt_of_pages)

def Print_All_Info(diction, format=False):
    for left, right in diction.items():
        if format:
            print ('{:<50}{}'.format(left, right))
        else:
            print (left, " --- ", right)
        

def Check_If_New_Day():
    if os.path.exists(REPO_PATH + "Time_logs_for_graphs\logs.txt"):
        with open(REPO_PATH + "Time_logs_for_graphs\logs.txt", "r") as file:
            for line in file:
                if datetime.today().strftime('%Y-%m-%d') in line:
                    return False
                
            return True
    else:
        print("error") # error handling (there may be an error during authentication)
        
def Info_Collecting(project):
    try:
        os.mkdir(REPO_PATH + "Time_logs_for_graphs")
    except: 
        pass # folder has already been created
    
    if Check_If_New_Day() == False:
        print("You have already called this function this day, please wait until tomorrow")
        return
    
    with open(REPO_PATH + "Time_logs_for_graphs\logs.txt", "a+") as file:
        log_string = datetime.today().strftime('%Y-%m-%d')
        log_string += "\n"
        log_string += str(len(project.issues) + 1)
        log_string += "\n"
        
        all_spent_time    = 0
        all_estimate_time = 0
        
        for cnt, elem in enumerate(project.issues):
            time_stats = elem.attributes["time_stats"]

            estimate = time_stats["time_estimate"]
            spent    = time_stats["total_time_spent"]
            
            all_estimate_time += estimate
            all_spent_time += spent
            
            log_string += str(project.issues_ids[cnt])
            log_string += "#"
            log_string += str(spent)
            log_string += "#"
            log_string += str(estimate)
            log_string += "\n"
            
        log_string += "0"  # can't be id of normal issue
        log_string += "#"
        log_string += str(all_spent_time)
        log_string += "#"
        log_string += str(all_estimate_time)
        log_string += "\n"
            
        
        file.write(log_string)

def Parce_Log_File(string):
    string_parsed = string.split("#", 3)
    return int(string_parsed[0]), int(string_parsed[1]), int(string_parsed[2])

def Draw_Collected_Graphs(project, id=0, mode=SHOW_MODE):
    if os.path.exists(REPO_PATH + "Time_logs_for_graphs\logs.txt"):
        with open(REPO_PATH + "Time_logs_for_graphs\logs.txt", "r") as file:
            start_log_date = file.readline().replace('\n', '')
           
        try: 
            created_date = Date_From_Str(project.issues_created_at[id])
        except:
            print("Invalid Id, possible ids are: ", [i for i in range(0, len(project.issues_ids))])
            return
            
        if (created_date - Date_From_Str(start_log_date)).days >= 0:
            last_event = created_date
        else:
            last_event = Date_From_Str(start_log_date)
            
        with open(REPO_PATH + "Time_logs_for_graphs\logs.txt", "r") as file:
            
            fig, ax = plt.subplots(figsize=STD_FIGSIZE)
            
            spent_x     = []
            estimate_x  = []
            remaining_x = []
            spent_y     = []
            estimate_y  = []
            remaining_y = []
                        
            if id == 0:
                search_id = 0
            else:
                try:
                    search_id = project.issues_ids[id - 1]
                except:
                    print("Invalid Id, possible ids are: ", [i for i in range(0, len(project.issues_ids))])
                    return
                
            pos = 0
            start_log_date = ''
            first = True
            
            for line in file:
                line = line.replace('\n', '')
                
                if first:
                    start_log_date = line
                    first = False
                
                if pos == 0:
                    if line.isdigit():
                        pos = int(line)
                    else:
                        date_now = line
                else:
                    pos -= 1
                              
                    file_id, spent, estimate = Parce_Log_File(line)

                    if file_id == search_id:
                        spent_x.    append(str((Date_From_Str(date_now) - last_event).days))
                        estimate_x. append(str((Date_From_Str(date_now) - last_event).days))
                        remaining_x.append(str((Date_From_Str(date_now) - last_event).days))
                        
                        spent_y.    append(spent    // 3600)
                        estimate_y. append(estimate // 3600)
                        remaining_y.append(estimate_y[-1] - spent_y[-1])
                    
            spent_x[0]     = "\n" + last_event.strftime("%d %B %Y")
            estimate_x[0]  = "\n" + last_event.strftime("%d %B %Y")
            remaining_x[0] = "\n" + last_event.strftime("%d %B %Y")
            
            x_names = [str(i) for i in range((project.date_today - last_event).days + 1)]
            x_names[0]  = "\n" + last_event.strftime("%d %B %Y")
            
            if x_names[-1] == spent_x[-1]:
                spent_x[-1]     = "\nToday"
                estimate_x[-1]  = "\nToday"
                remaining_x[-1] = "\nToday"
            
            x_names[-1] = "\nToday"

            
            plt.plot(estimate_x,  estimate_y,  color="r", label="Estimate time")
            plt.plot(remaining_x, remaining_y, color="g", label="Remaining time")
            plt.plot(spent_x,     spent_y,     color="b", label="Spent time")
            
            plt.xticks(x_names, fontsize=STD_FONTSIZE)
            ax.get_xticklabels()[0].set_color("m")
            ax.get_xticklabels()[-1].set_color("m")
            plt.yticks([i for i in range(0, estimate_y[0] + estimate_y[0] // 19 + 1, estimate_y[0] // 19 + 1)], fontsize=STD_FONTSIZE)

            if id == 0:
                plt.title("Stat for all issues", fontsize=TITLE_FONTSIZE)
            else:
                title = "Stat for #" + str(id) + " issue"
                plt.title(title, fontsize=TITLE_FONTSIZE)
            plt.xlabel("Number of passed days", fontsize=BIG_FONTSIZE) 
            plt.ylabel("Hours", fontsize=BIG_FONTSIZE)

            plt.legend(loc='center left', fontsize=BIG_FONTSIZE)

            if mode == SHOW_MODE: # draw graph
                plt.show()
                print()
            elif mode == PDF_MODE:  # add graph to pdf
                project.all_graphs.append(fig)
                plt.close()
            else:
                print("Error") # add error processing

def Draw_Legend(rect_colors, text_colors=[], mode=SHOW_MODE):
    
    if len(text_colors) == 0:
        text_colors = ['black'] * len(rect_colors)
        
    cnt_of_rows = len(rect_colors)
    
    coef = 0
    if cnt_of_rows > 30:
        fig, ax = plt.subplots(figsize=(17, cnt_of_rows // 3))
        coef = -0.05
    elif cnt_of_rows > 10:
        fig, ax = plt.subplots(figsize=(17, cnt_of_rows // 2))
    else:
        coef = 0.1
        fig, ax = plt.subplots(figsize=(17, cnt_of_rows))

    X, Y = fig.get_dpi() * fig.get_size_inches()
    height = Y / (cnt_of_rows + 1)
    width = X 

    for cnt, name in enumerate(rect_colors):
        row = cnt % cnt_of_rows
        col = cnt // cnt_of_rows
        y = Y - (row * height) - height

        xi_line = width * (col + 0.05)
        xf_line = width * (col + 0.15 + coef)
        xi_text = width * (col + 0.2 + coef)

        ax.text(xi_text, y, name, fontsize=BIG_FONTSIZE,
                horizontalalignment='left',
                verticalalignment='center', color=text_colors[cnt])

        ax.hlines(y + height * 0.1, xi_line, xf_line,
                  color=rect_colors[name], linewidth=(height * 0.5))

    ax.set_xlim(0, X)
    ax.set_ylim(0, Y)
    ax.set_axis_off()

    fig.subplots_adjust(left=0, right=1,
                        top=1, bottom=0,
                        hspace=0, wspace=0)
    
    if mode == SHOW_MODE: # draw graph
        plt.show()
    elif mode == PDF_MODE:
        plt.close()
        return fig # add to graph.all_graph
    else:
        print("Error") # add error processing

def Issues_Plot(project, issues_mode=ALL_ISSUES_MODE, mode=SHOW_MODE):
    if issues_mode == ALL_ISSUES_MODE:
        filtered_issues = project.percent_all_issues_done
    elif issues_mode == OPEN_ISSUES_MODE:
        filtered_issues = project.percent_open_issues_done
    elif issues_mode == CLOSED_ISSUES_MODE:
        filtered_issues = project.percent_closed_issues_done
    else:
        print ("Incorrect mode") # add error handling
        return
        
    fig, ax = plt.subplots(figsize=STD_FIGSIZE)

    ids_for_output = []
    for cnt, elem in enumerate(filtered_issues.keys()):
        if project.open_issues[project.issues_numbers[elem]] == False:
            ids_for_output.append(str(cnt + 1) + "\nCLOSED")
        else:
            ids_for_output.append(str(cnt + 1))

    percentage = []
    for i in filtered_issues.keys():
        percentage.append(filtered_issues[i])

    colors_column = []
    for i in filtered_issues.keys():
        if int(filtered_issues[i]) > 100:
            colors_column.append(DARKGREEN_RGB) # RED_RGB
        elif int(filtered_issues[i]) == 100:
            colors_column.append(GREEN_RGB)  
        else:
            colors_column.append(BLUE_RGB)

    if mode == SHOW_MODE:
        if COLOR_CONSOLE_SUPPORTED:
            Pr_Bla ("NUMBER \t\t\t\t NAME")
            print()
            for cnt, elem in enumerate(filtered_issues.keys()):
                if project.open_issues[project.issues_numbers[elem]] == False:
                    Pr_Red('{: =3}{}{}'.format(cnt + 1, ") ", project.issues_titles[elem]))
                    print()
                else:
                    Pr_Blu('{: =3}{}{}'.format(cnt + 1, ") ", project.issues_titles[elem]))
                    print()
                    
            Pr_Blu ("\n\t\t\t\tIn process tasks - blue")
            Pr_Red ("\t\t\t\tClosed tasks - red")
        else:
            print("NUMBER \t\t\t\t NAME")
            for cnt, elem in enumerate(filtered_issues.keys()):
                if project.open_issues[project.issues_numbers[elem]] == False:
                    print('{: =3}{}{}'.format(cnt + 1, ") ", project.issues_titles[elem]))
                else:
                    print('{: =3}{}{}'.format(cnt + 1, ") ", project.issues_titles[elem]))
    elif mode == PDF_MODE:
        text = dict()
        text_colors = []
        
        text["Num               Issues title"] = WHITE_RGB
        text_colors.append('black')

        for cnt, elem in enumerate(filtered_issues.keys()):
            if project.open_issues[project.issues_numbers[elem]] == False:
                text[str(cnt + 1) + ") " + str(project.issues_titles[elem])] = RED_RGB
                text_colors.append('red')
            else:
                text[str(cnt + 1) + ") " + str(project.issues_titles[elem])] = BLUE_RGB
                text_colors.append('blue')

    graph = plt.bar(ids_for_output, percentage, color=colors_column)

    plt.xticks(ids_for_output, fontsize=(STD_FONTSIZE - len(ids_for_output) // 300), rotation=60)
    for cnt, elem in enumerate(filtered_issues.keys()):
        if project.open_issues[project.issues_numbers[elem]] == False:
            ax.get_xticklabels()[cnt].set_color("red")

    plt.yticks([i * 10 for i in range(11)], fontsize=14)

    if issues_mode == ALL_ISSUES_MODE:
        plt.title("All issues progress",    fontsize=TITLE_FONTSIZE)
    elif issues_mode == OPEN_ISSUES_MODE:
        plt.title("Open issues progress",   fontsize=TITLE_FONTSIZE)
    elif issues_mode == CLOSED_ISSUES_MODE:
        plt.title("Closed issues progress", fontsize=TITLE_FONTSIZE)
        
    plt.xlabel("Number",     fontsize=BIG_FONTSIZE) 
    plt.ylabel("Percentage", fontsize=BIG_FONTSIZE)
    
    if mode == SHOW_MODE: # draw graph
        plt.show()
        print()
    elif mode == PDF_MODE:
        project.all_graphs.append(Draw_Legend(text, text_colors, PDF_MODE))
        project.all_graphs.append(fig) # add graph to pdf
        plt.close()
    else:
        print("Error") # add error processing
        
def Average_Issues_Time(project, include_zero=False, sorted=False, sorted_by="estimate", mode=SHOW_MODE):
    fig, ax = plt.subplots(figsize=STD_FIGSIZE)

    estimate_time = []
    spent_time    = []
    
    average_spent    = 0
    average_estimate = 0

    for cnt, elem in enumerate(project.open_issues):
        if not elem:
            time_stats = project.issues[cnt].attributes["time_stats"]

            estimate = time_stats["time_estimate"]
            spent    = time_stats["total_time_spent"]

            if spent == 0 and not include_zero:
                continue

            estimate_time.append(estimate / 60 / 60)
            spent_time.append(spent / 60 / 60)
    
    if sorted:
        if sorted_by == "estimate":
            for i in range(len(estimate_time)):
                for k in range(1, len(estimate_time) - i):
                    if estimate_time[k] < estimate_time[k - 1]:
                        (estimate_time[k], estimate_time[k - 1]) = (estimate_time[k - 1], estimate_time[k])
                        (spent_time[k],    spent_time[k - 1])    = (spent_time[k - 1],    spent_time[k])
        elif sorted_by == "spent":
            for i in range(len(spent_time)):
                for k in range(1, len(spent_time) - i):
                    if spent_time[k] < spent_time[k - 1]:
                        (spent_time[k],    spent_time[k - 1])    = (spent_time[k - 1],    spent_time[k])
                        (estimate_time[k], estimate_time[k - 1]) = (estimate_time[k - 1], estimate_time[k])
        else:
            if COLOR_CONSOLE_SUPPORTED:
                Pr_Red("Wrong parameter \"sorted_by\", possible variants are:\n")
                Pr_Bold("\"estimate\"")
                print("  if you want to sort by estimate time (default)")
                Pr_Bold("\"spent\"")
                print("  if you want to sort by spent time")
            else:
                print("Wrong parameter `sorted_by`, possible variants are:")
                print("\"estimate\" if you want to sort by estimate time (default)")
                print("\"spent\" if you want to sort by spent time")
                
            plt.close()
            return 
            
    average_estimate = sum(estimate_time) / len(estimate_time)
    average_spent    = sum(spent_time)    / len(spent_time)

    ax.bar([i + 0.8 for i in range(len(estimate_time))], estimate_time, width = 0.4, color='mediumblue', label='estimate time')
    ax.bar([i + 1.2 for i in range(len(spent_time))],    spent_time,    width = 0.4, color='c',          label='spent time')
    
    plt.xticks([i + 1 for i in range(len(estimate_time))])
    
    plt.hlines(average_estimate, 0, len(estimate_time), color='blue',     linestyle="dashed", label="average estimate")
    plt.hlines(average_spent,    0, len(spent_time),    color='darkcyan', linestyle="dashed", label="average spent")
    
    plt.title("Average issues time", fontsize=TITLE_FONTSIZE)
    plt.xlabel("Closed issues",      fontsize=BIG_FONTSIZE) 
    plt.ylabel("Hours",              fontsize=BIG_FONTSIZE)
    
    plt.legend(loc='upper center', fontsize=BIG_FONTSIZE)

    if mode == SHOW_MODE: # draw graph
        plt.show()
        print()
    elif mode == PDF_MODE:
        project.all_graphs.append(fig) # add graph to pdf
        plt.close()
    else:
        print("Error") # add error processing
        
def Estimate_X_Spent_Time(project, all_issues=False, mode=SHOW_MODE):
    fig, ax = plt.subplots(figsize=STD_FIGSIZE)

    estimate_per_day = []
    spent_per_day    = []
    
    estimate = 0
    spent    = 0

    for date in project.dates:
        if not all_issues:
            for cnt, elem in enumerate(project.open_issues):
                if not elem:
                    if project.issues[cnt].created_at[:10] == date:
                        estimate += project.issues[cnt].attributes["time_stats"]["time_estimate"]

                    if project.issues[cnt].closed_at[:10] == date:
                        spent += project.issues[cnt].attributes["time_stats"]["total_time_spent"]

        else:
            for cnt, elem in enumerate(project.open_issues):
                if project.issues[cnt].created_at[:10] == date:
                    estimate += project.issues[cnt].attributes["time_stats"]["time_estimate"]

                if project.issues[cnt].updated_at[:10] == date:
                    spent += project.issues[cnt].attributes["time_stats"]["total_time_spent"]

                
        estimate_per_day.append(estimate / 60 / 60)
        spent_per_day.append(spent / 60 / 60)
            


    plt.plot(project.dates, spent_per_day,    color="cyan",       label="Total spent time")
    plt.plot(project.dates, estimate_per_day, color="mediumblue", label="Total estimate time")

    plt.xticks([project.dates[i] for i in range(0, len(project.dates), 7)])

    plt.title("Time progress of closed issues", fontsize=TITLE_FONTSIZE)
    plt.xlabel("Date",                          fontsize=BIG_FONTSIZE) 
    plt.ylabel("Hours",                         fontsize=BIG_FONTSIZE)  

    plt.legend(fontsize=BIG_FONTSIZE)

    if mode == SHOW_MODE: # draw graph
        plt.show()
        print()
    elif mode == PDF_MODE:
        project.all_graphs.append(fig) # add graph to pdf
        plt.close()
    else:
        print("Error") # add error processing

def Cmp_For_Data(data):
    A = data.split()[-1]
    B = []
    for i in A:
        if i != '-':            
            B.append(int(i))
    return B

def Closed_X_Open_Issues(project, mode=SHOW_MODE):
    fig, ax = plt.subplots(figsize=STD_FIGSIZE)

    issues_per_day = dict()
    for cnt, elem in enumerate(project.issues_created_at):
        issues_per_day[elem] = project.issues_created_at.count(elem)

    closed_issues_per_day = dict()
    for cnt, elem in enumerate(project.issues_closed_at):
        closed_issues_per_day[elem] = project.issues_closed_at.count(elem)


    graph_y1 = [0]
    for cnt, elem in enumerate(issues_per_day.keys()):
        graph_y1.append(graph_y1[cnt] + issues_per_day[elem])

    graph_y2 = [0]
    for cnt, elem in enumerate(closed_issues_per_day.keys()):
        graph_y2.append(graph_y2[cnt] + closed_issues_per_day[elem])


    graph_y1 = graph_y1[1:]
    graph_y2 = graph_y2[1:]


    graph_x1 = [i for i in issues_per_day.keys()]
    graph_x1.sort(key=Cmp_For_Data)
    graph_x1_id = []
    for i in graph_x1:
        graph_x1_id.append((Date_From_Str(i) - project.start_date).days)


    graph_x2 = [i for i in closed_issues_per_day.keys()]
    graph_x2.sort(key=Cmp_For_Data)
    graph_x2_id = []
    for i in graph_x2:
        graph_x2_id.append((Date_From_Str(i) - project.start_date).days)


    graph_x12 = (graph_x2 + graph_x1)
    graph_x12.sort()


    plt.plot(graph_x1_id, graph_y1, color="b", label="Num of all issues")
    plt.plot(graph_x2_id, graph_y2, color="r", label="Num of closed issues")


    plt.xticks([i for i in range(0, len(project.dates), (len(project.dates) // 50 + 1))], fontsize=STD_FONTSIZE)
    plt.yticks([i for i in range(1, graph_y1[-1] + 6,   (graph_y1[-1] + 6) // 40 + 1)],   fontsize=STD_FONTSIZE)

    for i, v in enumerate(graph_y1):
        ax.annotate(str(v), xy=(graph_x1_id[i], v), xytext=(-7,7), textcoords='offset points', fontsize=BIG_FONTSIZE)

    for i, v in enumerate(graph_y2):
        ax.annotate(str(v), xy=(graph_x2_id[i], v), xytext=(-7,7), textcoords='offset points', fontsize=BIG_FONTSIZE)

    plt.title("Issues count",           fontsize=TITLE_FONTSIZE)
    plt.xlabel("Number of passed days", fontsize=BIG_FONTSIZE) 
    plt.ylabel("Number of issues",      fontsize=BIG_FONTSIZE)

    plt.legend(loc='upper left', fontsize=BIG_FONTSIZE)
    plt.text(-1.5, -1, project.start_date.strftime('%d %B %Y'), fontsize=STD_FONTSIZE, color="m")
    plt.text(plt.xlim()[1] - 0.5, -1, "Today",                  fontsize=STD_FONTSIZE, color="m")

    if mode == SHOW_MODE: # draw graph
        plt.show()
        print()
    elif mode == PDF_MODE:
        project.all_graphs.append(fig) # add graph to pdf
        plt.close()
    else:
        print("Error") # add error processing
        
def Print_Milestone_Info(milestone):
    print('{:<12}{}'.format("Id",          "= " + str(milestone.id)))
    print('{:<12}{}'.format("Local id",    "= " + str(milestone.iid)))
    print('{:<12}{}'.format("Project id",  "= " + str(milestone.project_id)))
    print('{:<12}{}'.format("Title",       "= " + str(milestone.title)))
    print('{:<12}{}'.format("Description", "= " + str(milestone.description.replace("#", ""))))
    print('{:<12}{}'.format("State",       "= " + str(milestone.state)))
    print('{:<12}{}'.format("Created at",  "= " + str(milestone.created_at[:10])))
    print('{:<12}{}'.format("Updated at",  "= " + str(milestone.updated_at[:10])))
    print('{:<12}{}'.format("Due date",    "= " + str(milestone.due_date)))
    print('{:<12}{}'.format("Start date",  "= " + str(milestone.start_date)))
    print('{:<12}{}'.format("Web url",     "= " + str(milestone.web_url)))

def Milestone_Related_Issues(project, milestone_id=0, milestone_title="", mode=SHOW_MODE): 
    milestone = None
    
    if milestone_title != "":
        for mil in project.milestones:
            if mil.title == milestone_title:
                milestone = mil
                break
        else:
            if COLOR_CONSOLE_SUPPORTED:
                Pr_Red("Wrong milestone title, possible titles are:\n")
                for mil in project.milestones:
                    print("Id:", end=" ")
                    Pr_Bold(mil.iid)
                    print(" Title:", end=" ")
                    Pr_Bold(mil.title)
                    print()
            else:
                print("Wrong milestone title, possible titles are:\n")
                for mil in project.milestones:
                    print("Id:", mil.iid, " Title: ", mil.title)
                
            return
    else:
        try:
            milestone = project.milestones[milestone_id - 1]
        except:
            if COLOR_CONSOLE_SUPPORTED:
                Pr_Red("Wrong id, possible ids are:\n")
                for mil in project.milestones:
                    print("Id:", end=" ")
                    Pr_Bold(mil.iid)
                    print(" Title:", end=" ")
                    Pr_Bold(mil.title)
                    print()
            else:
                print("Wrong id, possible ids are:\n")
                for mil in project.milestones:
                    print("Id:", mil.iid, " Title: ", mil.title)
            return
                
    
    fig, ax = plt.subplots(figsize=STD_FIGSIZE)

    issues_per_day = dict()
    cnt_now = 0
    was_drop = False

    for cnt, elem in enumerate(project.dates):
        for k in project.milestone_issues[milestone.iid - 1]:
            if k.created_at[:10] == elem:
                cnt_now += 1
            if k.closed_at != None and k.closed_at[:10] == elem:
                was_drop = True
                cnt_now -= 1

        if not was_drop:
            first_issues = cnt_now

        issues_per_day[elem] = cnt_now

    graph_y1 = [0]
    for cnt, elem in enumerate(issues_per_day.keys()):
        graph_y1.append(issues_per_day[elem])


    graph_y1 = graph_y1[1:]

    graph_x1 = [i for i in issues_per_day.keys()]
    graph_x1.sort(key=Cmp_For_Data)
    graph_x1_id = []


    plt.plot(graph_x1, graph_y1, color="b", label="Num of open issues")
    ax.plot([graph_x1[0], milestone.due_date], [first_issues, 0], 'r', linestyle = ':', linewidth=2, label="Milestone") 

    plt.xticks([graph_x1[i] for i in range(0, len(graph_x1), 7)], fontsize=STD_FONTSIZE)

    plt.title("Expected progress",      fontsize=TITLE_FONTSIZE)
    plt.xlabel("Date",                  fontsize=BIG_FONTSIZE) 
    plt.ylabel("Number of open issues", fontsize=BIG_FONTSIZE)

    plt.legend(loc='upper right', fontsize=BIG_FONTSIZE)

    if mode == SHOW_MODE: # draw graph
        plt.show()
        print()
    elif mode == PDF_MODE:
        project.all_graphs.append(fig) # add graph to pdf
        plt.close()
    else:
        print("Error") # add error processing        
        
def Helper_String_Format(str):
    return str

def Create_File_With_Description():
    with open(REPO_PATH + "Images/pdf_description.txt", "w+") as file:
        description = Helper_String_Format(
            TAB +
            "This pdf consists of graphs, which depend on your repository's issues. "
            "First three plots are graphs for all/closed/open issues accordingly. "
            "In them red color text corresponds to closed issues, and blue color text corresponds to open issues. "
            "At the same time blue\light green\dark green columns on all graphs mean that spent time is less\equal\more than estimate also accordingly(unless otherwise stated). ")
        
        file.write(description)

def Add_Description(project, mode=SHOW_MODE):
    fig, ax = plt.subplots(figsize=STD_FIGSIZE)
    
    X, Y = fig.get_dpi() * fig.get_size_inches()

    max_len = int(X / TITLE_FONTSIZE * 1.6)
    
    with open(REPO_PATH + "Images/pdf_description.txt") as file:
        text = file.readline()
    
    for i in range(len(text) + len(text) // int(max_len) * 2):
        if i < len(text) and i % max_len == 0:
            pos = i
            while text[pos] != ' ':
                pos -= 1
                if pos == -1:
                    text = text[:i] + '\n' + text[i:]
                    break
            else:
                text = text[:pos] + '\n' + text[pos:]
             
    ax.text(0, 1, text, fontsize=TITLE_FONTSIZE,
            horizontalalignment='left',
            verticalalignment='center')

    ax.set_axis_off()
    if mode == SHOW_MODE: # draw graph
        plt.show()
    elif mode == PDF_MODE:
        project.all_graphs.append(fig) # add graph to pdf
        plt.close()
    else:
        print("Error") # add error processing

def Make_Pdf(project, add_description=False, issues_plot=False, closed_x_open_issues=False, draw_collected_graphs=False, milestone_related_issues=False, average_issues_time=False, estimate_x_spent_time=False):
    if project.pdf_was_closed:
        if COLOR_CONSOLE_SUPPORTED:
            Pr_Red("You can only call this function once. If you want more, recreate a projectInfo class.")
        else:
            print("You can only call this function once. If you want more, recreate a projectInfo class.")
        return 
    
    project.all_graphs = []

    if add_description:
        Add_Description(project, mode=PDF_MODE)
    
    if issues_plot:
        Issues_Plot(project, ALL_ISSUES_MODE,    mode=PDF_MODE)
        Issues_Plot(project, OPEN_ISSUES_MODE,   mode=PDF_MODE)
        Issues_Plot(project, CLOSED_ISSUES_MODE, mode=PDF_MODE)
    
    if closed_x_open_issues:
        Closed_X_Open_Issues(project, mode=PDF_MODE)

    if draw_collected_graphs:
        for i in range(len(project.issues)):
            Draw_Collected_Graphs(project, i, mode=PDF_MODE)
    
    if milestone_related_issues:
        Milestone_Related_Issues(project, milestone_id=1, mode=PDF_MODE)
        
    if average_issues_time:
        Average_Issues_Time(project, mode=PDF_MODE)
        
    if estimate_x_spent_time:
        Estimate_X_Spent_Time(project, mode=PDF_MODE)

    for fig in project.all_graphs:
        project.pdf.savefig(fig)

    print("Number of pages: ", project.pdf.get_pagecount())
    project.pdf.close()
    project.pdf_was_closed = True
    
#=============================================================

def Print_All_Colors():
    colors = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)
    
    # Sort colors by hue, saturation, value and name.
    by_hsv = sorted((tuple(mcolors.rgb_to_hsv(mcolors.to_rgba(color)[:3])), name)
                    for name, color in colors.items())
    sorted_names = [name for hsv, name in by_hsv]
    
    n = len(sorted_names)
    ncols = 4
    nrows = n // ncols
    
    fig, ax = plt.subplots(figsize=(12, 10))
    
    # Get height and width
    X, Y = fig.get_dpi() * fig.get_size_inches()
    h = Y / (nrows + 1)
    w = X / ncols
    
    for i, name in enumerate(sorted_names):
        row = i % nrows
        col = i // nrows
        y = Y - (row * h) - h
    
        xi_line = w * (col + 0.05)
        xf_line = w * (col + 0.25)
        xi_text = w * (col + 0.3)
    
        ax.text(xi_text, y, name, fontsize=(h * 0.8),
                horizontalalignment='left',
                verticalalignment='center')
    
        ax.hlines(y + h * 0.1, xi_line, xf_line,
                  color=colors[name], linewidth=(h * 0.8))
        
    
    ax.set_xlim(0, X)
    ax.set_ylim(0, Y)
    ax.set_axis_off()
    
    fig.subplots_adjust(left=0, right=1,
                        top=1, bottom=0,
                        hspace=0, wspace=0)
    plt.show()