from ProjectInfo import * # import class ProjectInfo and plot functions

if __name__ == "__main__":
    gl = gb.Gitlab(GITLAB_URL, ACCESS_TOKEN) # create Gitlab object
    
    gl.auth() # authentication in gitlab
    
    project = gl.projects.get(PROJECT_ID) # get your project
    
    project_info = ProjectInfo(project) # now you can call all the functions
    
    # everything above is as in the first example
    
    ''' 
    signature: 
        Issues_Plot(project_info, issues_mode=ALL_ISSUES_MODE, mode=SHOW_MODE)
    
    where:
        project_info - any ProjectInfo class

        issues_mode - ALL_ISSUES_MODE or OPEN_ISSUES_MODE or CLOSED_ISSUES_MODE, creates histograms for all/open/closed issues accordingly
    
        mode - SHOW_MODE (default) or PDF_MODE, first draws a plot, second adds it to pdf
        
    briefly:
        creates histogramm for all/open/closed issues
    '''
 
    Issues_Plot(project_info, ALL_ISSUES_MODE)