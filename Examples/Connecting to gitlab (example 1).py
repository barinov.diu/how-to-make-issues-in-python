from ProjectInfo import * # import class ProjectInfo and plot functions

if __name__ == "__main__":
    gl = gb.Gitlab(GITLAB_URL, ACCESS_TOKEN) # create Gitlab object
    
    gl.auth() # authentication in gitlab
    
    project = gl.projects.get(PROJECT_ID) # get your project
    
    project_info = ProjectInfo(project) # now you can call all the functions