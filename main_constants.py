GITLAB_URL = 'https://gitlab.com'

REPO_PATH = ""  # your repository path

with open(REPO_PATH + "access_token.txt") as f:
    ACCESS_TOKEN = f.readline() # you can also write it right here (without using a file)
    
PROJECT_ID = 28449695   # your project id. To check if your id is correct, insert it after: https://gitlab.com/projects/
PROJECT_START_DATE = '' # your project start date in format: YYYY-MM-DD (e.g. '2021-12-31')

COLOR_CONSOLE_SUPPORTED = True  

BLACK_RGB  = [0, 0, 0]
RED_RGB    = [1, 0, 0]
GREEN_RGB  = [0, 1, 0]
YELLOW_RGB = [1, 1, 0]
BLUE_RGB   = [0, 0, 1]
WHITE_RGB  = [1, 1, 1]
DARKGREEN_RGB = [0, 100/255, 0]

BLA = 0
RED = 1
GRE = 2
YEL = 3
BLU = 4

def Reset_Color():
    print("\033[0m", end="")

def Pr_Bold(text, color=BLA):
    if color == BLA:
        print("\033[1m\033[{}m{}".format("30", text), end="")
        Reset_Color()
    elif color == RED:
        print("\033[1m\033[{}m{}".format("31", text), end="")
        Reset_Color()
    elif color == GRE:
        print("\033[1m\033[{}m{}".format("32", text), end="")
        Reset_Color()
    elif color == YEL:
        print("\033[1m\033[{}m{}".format("33", text), end="")
        Reset_Color()
    elif color == BLU:
        print("\033[1m\033[{}m{}".format("34", text), end="")
        Reset_Color()
    else:
        Pr_Red("Wrong color, possible colors are:")
        Pr_Bla("BLA")
        Pr_Red("RED")
        Pr_Gre("GRE")
        Pr_Yel("YEL")
        Pr_Blu("BLU")

def Pr_Bla(text):
    print("\033[30m{}".format(text), end="")
    Reset_Color()

def Pr_Red(text):
    print("\033[31m{}".format(text), end="")
    Reset_Color()
    
def Pr_Gre(text):
    print("\033[32m{}".format(text), end="")
    Reset_Color()
    
def Pr_Yel(text):
    print("\033[33m{}".format(text), end="")
    Reset_Color()
    
def Pr_Blu(text):
    print("\033[34m{}".format(text), end="")
    Reset_Color()    
    
ALL_ISSUES_MODE    = 0
OPEN_ISSUES_MODE   = 1
CLOSED_ISSUES_MODE = 2

SHOW_MODE = 0
PDF_MODE  = 1

STD_FONTSIZE   = 14
BIG_FONTSIZE   = 16
TITLE_FONTSIZE = 18

STD_FIGSIZE  = (15, 10)

TAB_LENGTH = 8
TAB = " " * TAB_LENGTH
