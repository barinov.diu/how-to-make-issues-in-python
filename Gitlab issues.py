from ProjectInfo import *

if __name__ == '__main__':
    gl = gb.Gitlab(GITLAB_URL, ACCESS_TOKEN)
    gl.auth()
    
    project = gl.projects.get(PROJECT_ID)
    
    project_info = ProjectInfo(project)
    
    #if not os.path.exists(REPO_PATH + "Images/pdf_description.txt"):
    #    Create_File_With_Description()
    
    #Info_Collecting(project_info) # used once a day
    #project_info.Print_All_Members()
    #Draw_Collected_Graphs(project_info, 23)
    
    #Issues_Plot(project_info, ALL_ISSUES_MODE)
    
    #Draw_Legend({"In process" : BLUE_RGB, "Spent hours = estimate" : GREEN_RGB, "Spent hours more than estimate" : DARKGREEN_RGB})   
    
    #Closed_X_Open_Issues(project_info)
    
    Make_Pdf(project_info, closed_x_open_issues=True, milestone_related_issues=True, average_issues_time=True, estimate_x_spent_time=True)