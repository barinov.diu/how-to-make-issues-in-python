import sys 
import os

sys.path.append(os.path.abspath("")) # insert your repo path

from ProjectInfo import *

if __name__ == '__main__':
    with open(REPO_PATH + "Time_logs_for_graphs\\log_dates.txt", "a+") as f:
        f.write((datetime.now()).strftime("%d-%m-%Y %H:%M") + '\n')
        
    gl = gb.Gitlab(GITLAB_URL, ACCESS_TOKEN)
    gl.auth()
    
    project = gl.projects.get(PROJECT_ID)
    
    project_info = ProjectInfo(project)
    
    if not os.path.exists(REPO_PATH + "Images\\pdf_description.txt"):
        Create_File_With_Description()
    
    Info_Collecting(project_info) # used once a day
    Make_Pdf(project_info, closed_x_open_issues=True, milestone_related_issues=True, average_issues_time=True, estimate_x_spent_time=True)