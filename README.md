# Issues monitoring

## Introduction

## Preparing
- [Get your access token](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#get-your-access-token)
- [Install libraries](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#install-libraries)
- [Install jupyter notebook (optional)](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#install-jupyter-notebook)
## Start working
1. [Change constants](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#change-constants)
2. [Look at the examples](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#look-at-the-examples)
## Functions
- Graphs
    * [Milestone_Related_Issues](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#milestone_related_issues)
    * [Closed_X_Open_Issues](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#closed_x_open_issues)
    * [Average_Issues_Time](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#average_issues_time)
    * [Estimate_X_Spent_Time](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#estimate_x_spent_time)
    * [Issues_Plot](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#issues_plot)
    * [Draw_Collected_Graphs](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#draw_collected_graphs)
- Auxiliary for graphs 
    * [Info_Collecting](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#info_collecting)
    * [Make_Pdf](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#make_pdf)
- Auxiliary
    * [Print_Milestone_Info](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#print_milestone_info)
    * [Print_All_Colors](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#print_all_colors)
    * [Draw_Legend](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#change_constants)
## Example of work
- [Example](/Images/Example_of_work.pdf)

### Get your access token 

At first you have to log in to your [gitlab](https://gitlab.com) account.

Then:

1. Сlick on the profile icon in the upper right corner
2. Choose `Edit profile`
3. Find and click on `Access Tokens` on the left
4. 
    * Select the first four items `(api, read_user, read_api, read_repository)`
    * Enter the `token name` (what name is **not important**)
    * You can also add `Expiration date`, but I prefer to leave it blank (never expires)

 ![Access tocken](/Images/How_to_get_personal_token.png)

5. Click on `Create personal access token` on the bottom

Сongratulations, now you have an `aссess token`! 

**Save it to yourself**, as it will disappear when the page is reloaded.

## Warning!
<ins>**DO NOT SHOW THE TOKEN TO ANYONE, IT PROVIDES ACCESS TO ALL YOUR PROJECTS**</ins>

<ins>_In particular, be careful when working with the git. You can inadvertently push the token and lose all your projects!_</ins>

If you still accidentally made a mistake, **immediately** revoke the token and create a new one.

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#preparing)

### Install libraries

In this project we will need the folowing libraries:
```python
import matplotlib
import gitlab
```
Make sure that you have them installed.
To install write this to the command line:

`pip install matplotlib`

`pip install python-gitlab`   **Not** ~~_pip install gitlab_~~ (We need <ins>_python_</ins> version)

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#preparing)

### Install jupyter notebook

You can see how to install it here: [Installation guide](https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html)
I prefer to use conda, but the choice is yours.

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#preparing)

---
---

### Change constants

At the beginning, we need to insert the local constants. So, open `main_constants.py` (after _git clone_, of course). In it, we need to change the following constants:
* `REPO_PATH`    -> your repository path 
* `ACCESS_TOKEN` -> your access token or make access_token.txt, in which it will be located
* `PROJECT_ID `  -> your project id, you can find it here:
![Project id](/Images/How_to_find_project_id.png)
* `PROJECT_START_DATE` -> your project start date (actually any date from which time will be counted on the graphs)
* `COLOR_CONSOLE_SUPPORTED` -> run this code with some text:
```python 
print("\033[31m {}" .format(text))
``` 
if it is red, set the parameter to TRUE

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#start-working)

### Look at the examples

There is an `"Examples"` folder in the repository. Look through and try to run them in order. (Also read comments)

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#start-working)

---
---

### Milestone_Related_Issues

+ Signature:
    ```python
    def Milestone_Related_Issues(project, milestone_id=0, milestone_title="", mode=SHOW_MODE)
    ```
    ***project*** - any `ProjectInfo` class

    ***milestone_id*** - milestone local id (on default 0, which means the last milestone)

    ***milestone_title*** - milestone title (if absent milestone_id is main)

    ***mode*** - `SHOW_MODE` or `PDF_MODE`. The first draws a plot, the second adds it to a pdf (on default SHOW_MODE)
+ Description:

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#functions)

### Closed_X_Open_Issues

+ Signature:
    ```python
    def Closed_X_Open_Issues(project, mode=SHOW_MODE)
    ```
    ***project*** - any `ProjectInfo` class

    ***mode*** - `SHOW_MODE` or `PDF_MODE`. The first draws a plot, the second adds it to a pdf (on default SHOW_MODE)
+ Description:

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#functions)

### Average_Issues_Time

+ Signature:
    ```python
    def Average_Issues_Time(project, include_zero=False, sorted=False, sorted_by="estimate", mode=SHOW_MODE)
    ```
    ***project*** - any `ProjectInfo` class

    ***include_zero*** - if True the graph consists of all columns (including columns with zero spent time), otherwise there will be columns only with non-zero spent time (on default False)

    ***sorted*** - if True columns will be sorted by 'sorted_by' parameter, otherwise the order is the same as for closed issues in the repository (on default False)

    ***sorted_by*** - `estimate` or `spent`. Shows by which parameter the sorting is performed (it does not affect in any way if 'sorted'=False) (on default "estimate")

    ***mode*** - `SHOW_MODE` or `PDF_MODE`. The first draws a plot, the second adds it to a pdf (on default SHOW_MODE)
+ Description:

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#functions)

### Estimate_X_Spent_Time

+ Signature:
    ```python
    def Estimate_X_Spent_Time(project, all_issues=False, mode=SHOW_MODE)
    ```
    ***project*** - any `ProjectInfo` class

    ***all_issues*** - if True, plot consists of all issues, otherwise only of closed (on default False)

    ***mode*** - `SHOW_MODE` or `PDF_MODE`. The first draws a plot, the second adds it to a pdf (on default SHOW_MODE)
+ Description:

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#functions)

### Issues_Plot

+ Signature:
    ```python
    def Issues_Plot(project, issues_mode, mode=SHOW_MODE)
    ```
    ***project*** - any `ProjectInfo` class

    ***issues_mode*** - `ALL_ISSUES_MODE`, `OPEN_ISSUES_MODE` or `CLOSED_ISSUES_MODE`. Creates histograms for all/open/closed issues accordingly

    ***mode*** - `SHOW_MODE` or `PDF_MODE`. The first draws a plot, the second adds it to a pdf (on default SHOW_MODE)
+ Description:

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#functions)

### Draw_Collected_Graphs

+ Signature:
    ```python
    def Draw_Collected_Graphs(project, id=0, mode=SHOW_MODE)
    ```
    ***project*** - any `ProjectInfo` class

    ***id*** - issue local id, value 0 means plot for all issues(in total) (on default 0)

    ***mode*** - `SHOW_MODE` or `PDF_MODE`. The first draws a plot, the second adds it to a pdf (on default SHOW_MODE)
+ Description:

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#functions)

### Info_Collecting

+ Signature:
    ```python
    def Info_Collecting(project)
    ```
    ***project*** - any `ProjectInfo` class
+ Description:

    Add issues info to file

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#functions)

### Make_Pdf

+ Signature:
    ```python
    def Make_Pdf(project, add_description=False, issues_plot=False, closed_x_open_issues=False, draw_collected_graphs=False, milestone_related_issues=False, average_issues_time=False, estimate_x_spent_time=False)
    ```
    ***project*** - any `ProjectInfo` class

    ***add_description*** - if True adds Add_Description(project, mode=PDF_MODE) to pdf file (on default False). It will be first page with description.

    ***issues_plot*** - if True adds [Issues_Plot](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#issues_plot)(project, mode=PDF_MODE) to pdf file (on default False)

    ***closed_x_open_issues*** - if True adds [Closed_X_Open_Issues](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#closed_x_open_issues)(project, mode=PDF_MODE) to pdf file (on default False)

    ***draw_collected_graphs*** - if True adds [Draw_Collected_Graphs](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#draw_collected_graphs)(project, mode=PDF_MODE) to pdf file (on default False)

    ***milestone_related_issues*** - if True adds [Milestone_Related_Issues](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#milestone_related_issues)(project, mode=PDF_MODE) to pdf file (on default False)

    ***average_issues_time*** - if True adds [Average_Issues_Time](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#average_issues_time)(project, mode=PDF_MODE) to pdf file (on default False)

    ***estimate_x_spent_time*** - if True adds [Estimate_X_Spent_Time](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#estimate_x_spent_time)(project, mode=PDF_MODE) to pdf file (on default False)

+ Description:

    Creates pdf file which contents graphs of selected functions

[Go back](https://gitlab.com/barinov.diu/issues-monitoring/-/blob/main/README.md#functions)
